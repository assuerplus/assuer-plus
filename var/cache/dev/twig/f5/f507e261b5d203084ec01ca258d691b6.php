<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* email/email.html.twig */
class __TwigTemplate_f23967eb7b1dfa0da59e24e41bdff7bb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "email/email.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "email/email.html.twig"));

        // line 1
        ob_start();
        // line 2
        echo "# Voici une déclaration de sinistre
---
## Informations sur l'assuré
- Numéro d'assuré : ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 5, $this->source); })()), "numberUser", [], "any", false, false, false, 5), "html", null, true);
        echo "
- Nom : ";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 6, $this->source); })()), "lastname", [], "any", false, false, false, 6), "html", null, true);
        echo "
- Prénom : ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 7, $this->source); })()), "firstname", [], "any", false, false, false, 7), "html", null, true);
        echo "
- Email : ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 8, $this->source); })()), "email", [], "any", false, false, false, 8), "html", null, true);
        echo "
- Numéro de téléphone: ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 9, $this->source); })()), "PhoneNumber", [], "any", false, false, false, 9), "html", null, true);
        echo "

--- 
## Information sur le sinistre
- Date du sinistre : ";
        // line 13
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["sinister"]) || array_key_exists("sinister", $context) ? $context["sinister"] : (function () { throw new RuntimeError('Variable "sinister" does not exist.', 13, $this->source); })()), "createdAt", [], "any", false, false, false, 13), "d/m/Y"), "html", null, true);
        echo "
- Adresse du sinistre : ";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["sinister"]) || array_key_exists("sinister", $context) ? $context["sinister"] : (function () { throw new RuntimeError('Variable "sinister" does not exist.', 14, $this->source); })()), "lieux", [], "any", false, false, false, 14), "html", null, true);
        echo "
- Déscription du sinsitre : ";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["sinister"]) || array_key_exists("sinister", $context) ? $context["sinister"] : (function () { throw new RuntimeError('Variable "sinister" does not exist.', 15, $this->source); })()), "description", [], "any", false, false, false, 15), "html", null, true);
        echo "
- Numéro de plaque d'immatriculation : ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["sinister"]) || array_key_exists("sinister", $context) ? $context["sinister"] : (function () { throw new RuntimeError('Variable "sinister" does not exist.', 16, $this->source); })()), "numberRegistration", [], "any", false, false, false, 16), "html", null, true);
        echo "

___

";
        $___internal_parse_0_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        echo $this->env->getRuntime('Twig\Extra\Markdown\MarkdownRuntime')->convert($___internal_parse_0_);
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "email/email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 1,  85 => 16,  81 => 15,  77 => 14,  73 => 13,  66 => 9,  62 => 8,  58 => 7,  54 => 6,  50 => 5,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% apply markdown_to_html %}
# Voici une déclaration de sinistre
---
## Informations sur l'assuré
- Numéro d'assuré : {{ user.numberUser }}
- Nom : {{ user.lastname }}
- Prénom : {{ user.firstname }}
- Email : {{ user.email }}
- Numéro de téléphone: {{ user.PhoneNumber }}

--- 
## Information sur le sinistre
- Date du sinistre : {{ sinister.createdAt | date('d/m/Y')}}
- Adresse du sinistre : {{ sinister.lieux }}
- Déscription du sinsitre : {{ sinister.description }}
- Numéro de plaque d'immatriculation : {{ sinister.numberRegistration }}

___

{% endapply %}", "email/email.html.twig", "/Users/ben/Documents/Projet Studi/Projet-3/assuer-plus/templates/email/email.html.twig");
    }
}
