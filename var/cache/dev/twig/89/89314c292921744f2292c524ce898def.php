<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user_interface/index.html.twig */
class __TwigTemplate_1f9c830b7f341ec6f03ba72cc5f5f83d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user_interface/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user_interface/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "user_interface/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello UserInterfaceController!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

    <section style=\"max-width: 800px;\" class=\"container d-flex flex-column justify-content-center align-items-center\">
        ";
        // line 10
        echo "
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "flashes", [], "any", false, false, false, 11));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 12
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 13
                echo "            <div class=\"alert alert-dismissible alert-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                <p> ";
                // line 14
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo " </p>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "      <div>
        <h1 class=\"mt-4 mb-2\">Bienvenue sur votre historique</h1>
        <p class=\"mb-2\">Vous trouverez ci-dessous l'historique de vos dernieres déclarations de sinistre</p>
      </div>
      
      <div class=\"d-flex flex-column justify-content-center\" style=\"max-width: 250px;\">
        <a class=\"btn btn-primary btn-sm\" type=\"button\" href=\"";
        // line 24
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_sinister");
        echo "\">Déclarer un sinistre</a>
      </div>

      <div class=\"d-flex flex-wrap justify-content-center mt-4\">
        ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sinisters"]) || array_key_exists("sinisters", $context) ? $context["sinisters"] : (function () { throw new RuntimeError('Variable "sinisters" does not exist.', 28, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["sinister"]) {
            // line 29
            echo "        <div class=\"card border-primary mb-3 me-sm-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header\">Sinistre du ";
            // line 30
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "createdAt", [], "any", false, false, false, 30), "d/m/Y"), "html", null, true);
            echo "</div>
            <div class=\"card-body d-flex flex-column\">
                <p class=\"card-title\">";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "lieux", [], "any", false, false, false, 32), "html", null, true);
            echo "</p>
                <p class=\"card-text\">";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "description", [], "any", false, false, false, 33), "html", null, true);
            echo "</p>
                <p class=\"card-text \">N° immatriculation : ";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "numberRegistration", [], "any", false, false, false, 34), "html", null, true);
            echo "</p>
                <p class=\"card-text \">Ville : ";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["sinister"], "city", [], "any", false, false, false, 35), "name", [], "any", false, false, false, 35), "html", null, true);
            echo "</p>
                <hr>
                ";
            // line 37
            $context["garageFound"] = false;
            // line 38
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["garages"]) || array_key_exists("garages", $context) ? $context["garages"] : (function () { throw new RuntimeError('Variable "garages" does not exist.', 38, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["garage"]) {
                // line 39
                echo "                    ";
                if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["sinister"], "city", [], "any", false, false, false, 39), "name", [], "any", false, false, false, 39) == twig_get_attribute($this->env, $this->source, $context["garage"], "ville", [], "any", false, false, false, 39)) &&  !(isset($context["garageFound"]) || array_key_exists("garageFound", $context) ? $context["garageFound"] : (function () { throw new RuntimeError('Variable "garageFound" does not exist.', 39, $this->source); })()))) {
                    // line 40
                    echo "                        ";
                    $context["garageFound"] = true;
                    // line 41
                    echo "                        <h5 class=\"card-title\">Garage le plus proche</h5>
                        <p class=\"card-text\">Adresse : ";
                    // line 42
                    echo twig_get_attribute($this->env, $this->source, $context["garage"], "adresse", [], "any", false, false, false, 42);
                    echo "</p>
                        <p class=\"card-text\">Ville : ";
                    // line 43
                    echo twig_get_attribute($this->env, $this->source, $context["garage"], "ville", [], "any", false, false, false, 43);
                    echo "</p>
                        <p class=\"card-text\">Téléphone : ";
                    // line 44
                    echo twig_get_attribute($this->env, $this->source, $context["garage"], "telephone", [], "any", false, false, false, 44);
                    echo "</p>
                    ";
                }
                // line 46
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['garage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "
                ";
            // line 48
            if ( !(isset($context["garageFound"]) || array_key_exists("garageFound", $context) ? $context["garageFound"] : (function () { throw new RuntimeError('Variable "garageFound" does not exist.', 48, $this->source); })())) {
                // line 49
                echo "                    <p class=\"card-text\">Aucun garage trouvé.</p>
                ";
            }
            // line 51
            echo "
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sinister'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "      </div>

    </section>

  
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "user_interface/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 55,  213 => 51,  209 => 49,  207 => 48,  204 => 47,  198 => 46,  193 => 44,  189 => 43,  185 => 42,  182 => 41,  179 => 40,  176 => 39,  171 => 38,  169 => 37,  164 => 35,  160 => 34,  156 => 33,  152 => 32,  147 => 30,  144 => 29,  140 => 28,  133 => 24,  125 => 18,  119 => 17,  110 => 14,  105 => 13,  100 => 12,  96 => 11,  93 => 10,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello UserInterfaceController!{% endblock %}

{% block body %}


    <section style=\"max-width: 800px;\" class=\"container d-flex flex-column justify-content-center align-items-center\">
        {# display a flash message #}

        {% for label, messages in app.flashes %}
        {% for message in messages %}
            <div class=\"alert alert-dismissible alert-{{ label }}\">
                <p> {{ message }} </p>
            </div>
        {% endfor %}
        {% endfor %}
      <div>
        <h1 class=\"mt-4 mb-2\">Bienvenue sur votre historique</h1>
        <p class=\"mb-2\">Vous trouverez ci-dessous l'historique de vos dernieres déclarations de sinistre</p>
      </div>
      
      <div class=\"d-flex flex-column justify-content-center\" style=\"max-width: 250px;\">
        <a class=\"btn btn-primary btn-sm\" type=\"button\" href=\"{{ path('app_sinister')}}\">Déclarer un sinistre</a>
      </div>

      <div class=\"d-flex flex-wrap justify-content-center mt-4\">
        {% for sinister in sinisters %}
        <div class=\"card border-primary mb-3 me-sm-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header\">Sinistre du {{ sinister.createdAt | date('d/m/Y') }}</div>
            <div class=\"card-body d-flex flex-column\">
                <p class=\"card-title\">{{ sinister.lieux }}</p>
                <p class=\"card-text\">{{ sinister.description }}</p>
                <p class=\"card-text \">N° immatriculation : {{ sinister.numberRegistration }}</p>
                <p class=\"card-text \">Ville : {{ sinister.city.name }}</p>
                <hr>
                {% set garageFound = false %}
                {% for garage in garages %}
                    {% if sinister.city.name == garage.ville and not garageFound %}
                        {% set garageFound = true %}
                        <h5 class=\"card-title\">Garage le plus proche</h5>
                        <p class=\"card-text\">Adresse : {{ garage.adresse|raw }}</p>
                        <p class=\"card-text\">Ville : {{ garage.ville|raw }}</p>
                        <p class=\"card-text\">Téléphone : {{ garage.telephone|raw }}</p>
                    {% endif %}
                {% endfor %}

                {% if not garageFound %}
                    <p class=\"card-text\">Aucun garage trouvé.</p>
                {% endif %}

            </div>
        </div>
        {% endfor %}
      </div>

    </section>

  
{% endblock %}
", "user_interface/index.html.twig", "/Users/ben/Documents/Projet Studi/Projet-3/assuer-plus/templates/user_interface/index.html.twig");
    }
}
