<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* assure_interface/index.html.twig */
class __TwigTemplate_9b5277660aa193916c1f53c5a8ff21ab extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "assure_interface/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "assure_interface/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "assure_interface/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Dashboard";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

    <section style=\"max-width: 800px;\" class=\"container d-flex flex-column justify-content-center align-items-center\">
        ";
        // line 10
        echo "
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "flashes", [], "any", false, false, false, 11));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 12
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 13
                echo "            <div class=\"alert alert-dismissible alert-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                <p> ";
                // line 14
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo " </p>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "      <div>
        <h1 class=\"mt-4 mb-2\">Bienvenue sur votre historique</h1>
        <p class=\"mb-2\">Vous trouverez ci-dessous l'historique de vos dernieres déclarations de sinistre</p>
      </div>
      
      <div class=\"d-flex flex-column justify-content-center\" style=\"max-width: 250px;\">
        <a class=\"btn btn-primary btn-sm\" type=\"button\" href=\"";
        // line 24
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("form_sinister");
        echo "\">Déclarer un sinistre</a>
      </div>

      <div class=\"d-flex flex-wrap justify-content-center mt-4\">
        ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 28, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["sinister"]) {
            // line 29
            echo "        <div class=\"card border-primary mb-3 me-sm-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header\">Sinistre du ";
            // line 30
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "createdAt", [], "any", false, false, false, 30), "d/m/Y"), "html", null, true);
            echo "</div>
            <div class=\"card-body d-flex flex-column\">
                <p class=\"card-title\">";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "lieux", [], "any", false, false, false, 32), "html", null, true);
            echo "</p>
                <p class=\"card-text\">";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "description", [], "any", false, false, false, 33), "html", null, true);
            echo "</p>
                <p class=\"card-text \">N° immatriculation : ";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sinister"], "numberRegistration", [], "any", false, false, false, 34), "html", null, true);
            echo "</p>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sinister'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "      </div>
     <div class=\"navigation\">
    ";
        // line 40
        echo $this->env->getRuntime('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationRuntime')->render($this->env, (isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 40, $this->source); })()));
        echo "
  </div>
    </section>
  </div>
  
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "assure_interface/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 40,  170 => 38,  160 => 34,  156 => 33,  152 => 32,  147 => 30,  144 => 29,  140 => 28,  133 => 24,  125 => 18,  119 => 17,  110 => 14,  105 => 13,  100 => 12,  96 => 11,  93 => 10,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Dashboard{% endblock %}

{% block body %}


    <section style=\"max-width: 800px;\" class=\"container d-flex flex-column justify-content-center align-items-center\">
        {# display a flash message #}

        {% for label, messages in app.flashes %}
        {% for message in messages %}
            <div class=\"alert alert-dismissible alert-{{ label }}\">
                <p> {{ message }} </p>
            </div>
        {% endfor %}
        {% endfor %}
      <div>
        <h1 class=\"mt-4 mb-2\">Bienvenue sur votre historique</h1>
        <p class=\"mb-2\">Vous trouverez ci-dessous l'historique de vos dernieres déclarations de sinistre</p>
      </div>
      
      <div class=\"d-flex flex-column justify-content-center\" style=\"max-width: 250px;\">
        <a class=\"btn btn-primary btn-sm\" type=\"button\" href=\"{{ path('form_sinister')}}\">Déclarer un sinistre</a>
      </div>

      <div class=\"d-flex flex-wrap justify-content-center mt-4\">
        {% for sinister in pagination %}
        <div class=\"card border-primary mb-3 me-sm-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header\">Sinistre du {{ sinister.createdAt | date('d/m/Y') }}</div>
            <div class=\"card-body d-flex flex-column\">
                <p class=\"card-title\">{{ sinister.lieux }}</p>
                <p class=\"card-text\">{{ sinister.description }}</p>
                <p class=\"card-text \">N° immatriculation : {{ sinister.numberRegistration }}</p>
            </div>
        </div>
        {% endfor %}
      </div>
     <div class=\"navigation\">
    {{ knp_pagination_render(pagination) }}
  </div>
    </section>
  </div>
  
{% endblock %}
", "assure_interface/index.html.twig", "/Users/ben/Documents/Projet Studi/Projet-3/assuer-plus/templates/assure_interface/index.html.twig");
    }
}
