<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container6ysDk4L\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container6ysDk4L/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/Container6ysDk4L.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\Container6ysDk4L\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \Container6ysDk4L\App_KernelDevDebugContainer([
    'container.build_hash' => '6ysDk4L',
    'container.build_id' => '4f76cb55',
    'container.build_time' => 1685365771,
], __DIR__.\DIRECTORY_SEPARATOR.'Container6ysDk4L');
