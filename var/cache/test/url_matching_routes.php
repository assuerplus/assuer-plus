<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\Admin\\DashboardController::index'], null, null, null, false, false, null]],
        '/garage-reparation' => [[['_route' => 'app_garage_reparation_index', '_controller' => 'App\\Controller\\GarageReparationController::index'], null, ['GET' => 0], null, true, false, null]],
        '/garage-reparation/new' => [[['_route' => 'app_garage_reparation_new', '_controller' => 'App\\Controller\\GarageReparationController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/' => [[['_route' => 'app_home', '_controller' => 'App\\Controller\\HomePageController::index'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegisterController::register'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/declaration' => [[['_route' => 'app_sinister', '_controller' => 'App\\Controller\\SinisterController::index'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/moncompte' => [[['_route' => 'app_user_historique', '_controller' => 'App\\Controller\\UserInterfaceController::index'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/garage\\-reparation/([^/]++)(?'
                    .'|(*:38)'
                    .'|/edit(*:50)'
                    .'|(*:57)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => 'app_garage_reparation_show', '_controller' => 'App\\Controller\\GarageReparationController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        50 => [[['_route' => 'app_garage_reparation_edit', '_controller' => 'App\\Controller\\GarageReparationController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        57 => [
            [['_route' => 'app_garage_reparation_delete', '_controller' => 'App\\Controller\\GarageReparationController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
