<?php

namespace ContainerTB1VPF6;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getDeclarationSinisterTypeService extends App_KernelTestDebugContainer
{
    /**
     * Gets the private 'App\Form\DeclarationSinisterType' shared autowired service.
     *
     * @return \App\Form\DeclarationSinisterType
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
        include_once \dirname(__DIR__, 4).'/src/Form/DeclarationSinisterType.php';

        return $container->privates['App\\Form\\DeclarationSinisterType'] = new \App\Form\DeclarationSinisterType();
    }
}
