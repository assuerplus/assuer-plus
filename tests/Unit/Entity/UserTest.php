<?php

namespace App\Tests\Entity;

use App\Entity\Sinister;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{


    public function testGettersAndSetters(): void
    {
        $user = new User();

        $user->setEmail('test@example.com');
        $this->assertEquals('test@example.com', $user->getEmail());

        $user->setNumberUser('123456');
        $this->assertEquals('123456', $user->getNumberUser());

        $user->setFirstname('John');
        $this->assertEquals('John', $user->getFirstname());

        $user->setLastname('Doe');
        $this->assertEquals('Doe', $user->getLastname());

        $user->setPhoneNumber('1234567890');
        $this->assertEquals('1234567890', $user->getPhoneNumber());

        $this->assertEquals('test@example.com', $user->getUserIdentifier());

        $user->setRoles(['ROLE_USER']);
        $this->assertEquals(['ROLE_USER'], $user->getRoles());

        $user->setPassword('password');
        $this->assertEquals('password', $user->getPassword());


    }

   
}
