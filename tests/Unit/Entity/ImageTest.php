<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Images;
use App\Entity\Sinister;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\File;

class ImagesTest extends TestCase
{

    public function testSetAndGetImageFile(): void
    {
        $images = new Images();
        $file = new File(__DIR__.'/test_image.jpeg');

        $images->setImageFile($file);
        $this->assertSame($file, $images->getImageFile());
    }

    public function testSetAndGetFilename(): void
    {
        $images = new Images();
        $filename = 'test_image.jpeg';

        $images->setFilename($filename);
        $this->assertSame($filename, $images->getFilename());
    }

    public function testSetAndGetUpdatedAt(): void
    {
        $images = new Images();
        $updatedAt = new \DateTimeImmutable('2023-05-23');

        $images->setUpdatedAt($updatedAt);
        $this->assertSame($updatedAt, $images->getUpdatedAt());
    }

    public function testSetAndGetSinister(): void
    {
        $images = new Images();
        $sinister = new Sinister(); // Assuming you have a Sinister entity

        $images->setSinister($sinister);
        $this->assertSame($sinister, $images->getSinister());
    }
}
