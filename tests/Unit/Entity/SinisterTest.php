<?php

namespace App\Tests\Entity;

use App\Entity\Sinister;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SinisterTest extends KernelTestCase
{
    public function testConstructor()
    {
        $sinister = new Sinister();
        $this->assertNull($sinister->getId());
        $this->assertNull($sinister->getNumberRegistration());
        $this->assertNull($sinister->getLieux());
        $this->assertNull($sinister->getDescription());
    }

    public function testNumberRegistration()
    {
        $sinister = new Sinister();
        $sinister->setNumberRegistration('123456789');
        $this->assertSame('123456789', $sinister->getNumberRegistration());
    }

    public function testAdressOfSinister()
    {
        $sinister = new Sinister();
        $sinister->setLieux('1 rue de la paix');
        $this->assertSame('1 rue de la paix', $sinister->getLieux());
    }

    public function testDescription()
    {
        $sinister = new Sinister();
        $sinister->setDescription('description');
        $this->assertSame('description', $sinister->getDescription());
    }

    public function testCreatedAt()
    {
        $sinister = new Sinister();
        $sinister->setCreatedAt(new \DateTimeImmutable());
        $this->assertInstanceOf(\DateTimeImmutable::class, $sinister->getCreatedAt());
    }
}
