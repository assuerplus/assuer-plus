<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230528223453 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE garage_reparation (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, adresse LONGTEXT NOT NULL, telephone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, site_web VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, heures_ouverture TIME DEFAULT NULL, heure_fermeture TIME DEFAULT NULL, services_offerts LONGTEXT DEFAULT NULL, ville VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE images (id INT AUTO_INCREMENT NOT NULL, sinister_id INT NOT NULL, filename VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_E01FBE6A3E67426B (sinister_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE remorquage_automobile (id INT AUTO_INCREMENT NOT NULL, garage_reparation_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, adresse LONGTEXT NOT NULL, telephone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, heures_ouverture TIME DEFAULT NULL, heure_fermeture TIME DEFAULT NULL, INDEX IDX_556CA84387973728 (garage_reparation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sinister (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, garage_reparation_id INT DEFAULT NULL, remorquage_automobile_id INT DEFAULT NULL, city_id INT NOT NULL, type_id INT DEFAULT NULL, lieux VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, number_registration VARCHAR(9) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', marque VARCHAR(255) NOT NULL, INDEX IDX_73FC7B36A76ED395 (user_id), UNIQUE INDEX UNIQ_73FC7B3687973728 (garage_reparation_id), UNIQUE INDEX UNIQ_73FC7B363647ED6E (remorquage_automobile_id), INDEX IDX_73FC7B368BAC62AF (city_id), INDEX IDX_73FC7B36C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sinister_sinister (sinister_source INT NOT NULL, sinister_target INT NOT NULL, INDEX IDX_DF45250AF8A147B4 (sinister_source), INDEX IDX_DF45250AE144173B (sinister_target), PRIMARY KEY(sinister_source, sinister_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, marque VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, number_user VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, lastname VARCHAR(255) DEFAULT NULL, firstname VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A3E67426B FOREIGN KEY (sinister_id) REFERENCES sinister (id)');
        $this->addSql('ALTER TABLE remorquage_automobile ADD CONSTRAINT FK_556CA84387973728 FOREIGN KEY (garage_reparation_id) REFERENCES garage_reparation (id)');
        $this->addSql('ALTER TABLE sinister ADD CONSTRAINT FK_73FC7B36A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sinister ADD CONSTRAINT FK_73FC7B3687973728 FOREIGN KEY (garage_reparation_id) REFERENCES garage_reparation (id)');
        $this->addSql('ALTER TABLE sinister ADD CONSTRAINT FK_73FC7B363647ED6E FOREIGN KEY (remorquage_automobile_id) REFERENCES remorquage_automobile (id)');
        $this->addSql('ALTER TABLE sinister ADD CONSTRAINT FK_73FC7B368BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE sinister ADD CONSTRAINT FK_73FC7B36C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE sinister_sinister ADD CONSTRAINT FK_DF45250AF8A147B4 FOREIGN KEY (sinister_source) REFERENCES sinister (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sinister_sinister ADD CONSTRAINT FK_DF45250AE144173B FOREIGN KEY (sinister_target) REFERENCES sinister (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A3E67426B');
        $this->addSql('ALTER TABLE remorquage_automobile DROP FOREIGN KEY FK_556CA84387973728');
        $this->addSql('ALTER TABLE sinister DROP FOREIGN KEY FK_73FC7B36A76ED395');
        $this->addSql('ALTER TABLE sinister DROP FOREIGN KEY FK_73FC7B3687973728');
        $this->addSql('ALTER TABLE sinister DROP FOREIGN KEY FK_73FC7B363647ED6E');
        $this->addSql('ALTER TABLE sinister DROP FOREIGN KEY FK_73FC7B368BAC62AF');
        $this->addSql('ALTER TABLE sinister DROP FOREIGN KEY FK_73FC7B36C54C8C93');
        $this->addSql('ALTER TABLE sinister_sinister DROP FOREIGN KEY FK_DF45250AF8A147B4');
        $this->addSql('ALTER TABLE sinister_sinister DROP FOREIGN KEY FK_DF45250AE144173B');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE garage_reparation');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE remorquage_automobile');
        $this->addSql('DROP TABLE sinister');
        $this->addSql('DROP TABLE sinister_sinister');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
