# Projet-Symfony-assurerPlus

## Installation

### 1. Clonez le projet

Commencez par cloner le projet depuis le repository gitlab.

### 2. Installez les dépendances

composer install

### 3. Créez la base de données

php bin/console doctrine:database:create

### 4. Créez les tables

php bin/console doctrine:schema:update --force

### 5. Importez les fixtures

php bin/console doctrine:fixtures:load

### 6. Lancez le serveur

php bin/console server:run

### 7. Connectez-vous

Rendez-vous sur la page /login pour vous connecter avec les identifiants suivants :

- admin / admin (rôle ROLE_ADMIN)
- user / user (rôle ROLE_USER)

### 8. Profitez !
