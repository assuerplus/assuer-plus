<?php

namespace App\Controller;

use App\Form\SinisterType;
use App\Repository\GarageReparationRepository;
use App\Repository\RemorquageAutomobileRepository;
use App\Repository\SinisterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\File;
use Symfony\Component\Routing\Annotation\Route;

class SinisterController extends AbstractController
{
    #[Route('/declaration', name: 'app_sinister', methods: ['GET', 'POST'])]
    public function index(Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        SinisterRepository $sinister,
        RemorquageAutomobileRepository $automobileRepository,
        GarageReparationRepository $garageReparationRepository): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(SinisterType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sinister = $form->getData();
            $sinister->setUser($user);
            $sinister->setMarque($form->get('marque')->getData());
            $sinister->setType($form->get('type')->getData());
            $entityManager->persist($sinister);
            $entityManager->flush();

            $images = $sinister->getImages();

            $email = (new TemplatedEmail())
                ->from('admin@assurplus.fr')
                ->to('admin@studi.fr')
                ->subject('test envoie mail')
                ->htmlTemplate('email/email.html.twig')
                ->context([
                    'sinister' => $sinister,
                    'user' => $user,
                ]);

            foreach ($images as $image) {
                $email->addPart(new DataPart(new File($image->getImageFile()->getRealPath())));
            }

            // $mailer->send($email);

            $this->addFlash('success', 'Votre déclaration nous a bien été transmise');

            return $this->redirectToRoute('app_user_historique');
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Il y a des erreurs dans la saisi du formulaire.');
        }

        return $this->render('sinister/index.html.twig', ['form' => $form]);
    }
}
