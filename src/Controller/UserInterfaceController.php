<?php

namespace App\Controller;

use App\Repository\GarageReparationRepository;
use App\Repository\RemorquageAutomobileRepository;
use App\Repository\SinisterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserInterfaceController extends AbstractController
{
    #[Route('/moncompte', name: 'app_user_historique')]
    public function index(
        SinisterRepository $sinisterRepository,
        GarageReparationRepository $garage,
        RemorquageAutomobileRepository $remorquage
    ): Response {
        return $this->render('user_interface/index.html.twig', [
            'user' => $this->getUser(),
            'sinisters' => $sinisterRepository->findAllOrderByCreatedAt(),
            'garages' => $garage->findAll(),
            'remorquages' => $remorquage->findAll(),
        ]);
    }
}
