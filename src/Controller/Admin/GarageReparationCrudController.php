<?php

namespace App\Controller\Admin;

use App\Entity\GarageReparation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;

class GarageReparationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return GarageReparation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Garage Reparation')
            ->setEntityLabelInPlural('Garage Reparations')
            ->setPaginatorPageSize(10)
            ->setDefaultSort(['id' => 'ASC'])
            ->setDateFormat('dd/MM/yyyy')
            ->setTimeFormat('HH:mm:ss')
            ->setDateTimeFormat('dd/MM/yyyy HH:mm:ss')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addPanel('Details');

        if (Crud::PAGE_INDEX === $pageName || Crud::PAGE_DETAIL === $pageName) {
            yield IdField::new('id')->hideOnForm()->hideOnIndex();
            yield TextField::new('nom');
            yield TextEditorField::new('adresse');
            yield TextField::new('ville');
            yield TextField::new('telephone');
            yield EmailField::new('email');
            yield TextField::new('siteWeb');
            yield TextEditorField::new('description');
            yield TimeField::new('heuresOuverture');
            yield TimeField::new('heureFermeture');
            yield TextEditorField::new('servicesOfferts');
            yield AssociationField::new('remorquageAutomobile')->hideOnIndex();
        } else {
            yield TextField::new('nom');
            yield TextEditorField::new('adresse');
            yield TextField::new('ville');
            yield TextField::new('telephone');
            yield EmailField::new('email');
            yield TextField::new('siteWeb');
            yield TextEditorField::new('description');
            yield TimeField::new('heuresOuverture');
            yield TimeField::new('heureFermeture');
            yield TextEditorField::new('servicesOfferts');
        }
    }
}
