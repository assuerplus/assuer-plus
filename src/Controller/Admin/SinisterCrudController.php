<?php

namespace App\Controller\Admin;

use App\Entity\Sinister;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SinisterCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Sinister::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
