<?php

namespace App\Controller\Admin;

use App\Entity\RemorquageAutomobile;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;

class RemorquageAutomobileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RemorquageAutomobile::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Remorquage Automobile')
            ->setEntityLabelInPlural('Remorquage Automobiles')
            ->setPaginatorPageSize(10)
            ->setDefaultSort(['id' => 'ASC'])
            ->setDateFormat('dd/MM/yyyy')
            ->setTimeFormat('HH:mm:ss')
            ->setDateTimeFormat('dd/MM/yyyy HH:mm:ss')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm()->hideOnIndex();
        yield FormField::addPanel('Contact information');
        yield TextField::new('nom');
        yield EmailField::new('email');
        yield TextEditorField::new('adresse');
        yield TextField::new('telephone');
        yield TimeField::new('heuresOuverture');
        yield TimeField::new('heureFermeture');
        yield AssociationField::new('GarageReparation');
    }
}
