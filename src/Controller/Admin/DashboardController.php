<?php

namespace App\Controller\Admin;

use App\Entity\GarageReparation;
use App\Entity\RemorquageAutomobile;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator
    ) {
    }

    #[Route('/admin', name: 'admin_dashboard')]
    public function index(): Response
    {
        $url = $this->adminUrlGenerator->setController(GarageReparationCrudController::class)->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Assuer Plus');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Accueil', 'fas fa-home', 'app_home');
        yield MenuItem::linkToRoute('Déclaration de sinistre', 'fas fa-car-crash', 'app_sinister');
        yield MenuItem::linkToCrud('Garage', 'fas fa-list', GarageReparation::class);
        yield MenuItem::linkToCrud('Remorquage', 'fas fa-list', RemorquageAutomobile::class);
    }
}
