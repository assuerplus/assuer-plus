<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class DeclarationSinisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('customerPart', UserType::class, [
                'label' => 'Information du client',
            ])
            ->add('sinisterPart', SinisterType::class, [
                'label' => 'Information sur le sinsitre ',
            ])
            ->add('submit', SubmitType::class);
    }
}
