<?php

namespace App\Form;

use App\Entity\GarageReparation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class GarageReparationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('nom', TextType::class, [
            'label' => 'Nom du garage de réparation',
        ])
        ->add('adresse', TextType::class, [
            'label' => 'Adresse',
        ])
        ->add('telephone', TextType::class, [
            'label' => 'Numéro de téléphone',
        ])
        ->add('email', EmailType::class, [
            'label' => 'Adresse e-mail',
            'required' => false,
        ])
        ->add('siteWeb', UrlType::class, [
            'label' => 'Site web',
            'required' => false,
        ])
        ->add('description', TextareaType::class, [
            'label' => 'Description',
            'required' => false,
        ])
        ->add('heuresOuverture', TextType::class, [
            'label' => 'Heures d\'ouverture',
            'required' => false,
        ])
        ->add('servicesOfferts', TextareaType::class, [
            'label' => 'Services offerts',
            'required' => false,
        ])
        ->add('noteMoyenne', NumberType::class, [
            'label' => 'Note moyenne',
            'required' => false,
        ])
        ->add('latitude', NumberType::class, [
            'label' => 'Latitude',
            'required' => false,
        ])
        ->add('longitude', NumberType::class, [
            'label' => 'Longitude',
            'required' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GarageReparation::class,
        ]);
    }
}
