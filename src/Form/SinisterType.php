<?php

namespace App\Form;

use App\Entity\Sinister;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SinisterType extends AbstractType
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lieux', TextType::class, [
                'label' => 'Adresse du sinistre',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description du sinistre',
            ])
            ->add('numberRegistration', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Ex:  AB 123 CD',
                ],
                'label' => 'Numéro d\'immatriculation de votre voiture',
            ])
            ->add('imagesFiles', FileType::class, [
                'required' => true,
                'multiple' => true,
                'label' => 'Envoyer vos photographies',
            ])
            ->add('city', EntityType::class, [
                'class' => 'App\Entity\City',
                'choice_label' => 'name',
                'label' => 'Ville',
            ])
            ->add('type', EntityType::class, [
                'class' => 'App\Entity\Type',
                'choice_label' => 'nom',
                'label' => 'Type de vehicule',
            ])
            ->add('marque', EntityType::class, [
                'class' => 'App\Entity\Type',
                'choice_label' => 'marque',
                'label' => 'Marque de vehicule',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Sinister::class,
        ]);
    }
}
