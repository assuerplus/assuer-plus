<?php

namespace App\Service;

use App\Entity\City;
use App\Repository\CityRepository;
use Symfony\Component\Console\Style\SymfonyStyle;

class CsvConverterService
{
    public function __construct(
        private \Doctrine\ORM\EntityManagerInterface $entityManager,
        private CityRepository $cityRepository
    ) {
    }

    public function ImportCitites(SymfonyStyle $io)
    {
        $io->title('Importing cities from CSV file');
        $csv = array_map('str_getcsv', file('data/cities.csv'));

        return $this->CreateOrUpdate($io, $csv, City::class);
    }

    public function CreateOrUpdate(SymfonyStyle $io, array $csv, string $class)
    {
        $batchSize = 100; // Taille du lot

        $io->progressStart(count($csv));
        // $inseeCode $cityCode $zipCode $label $latitude $longitude $departmentName $departmentNumber $regionName $regionGeoJsonName
        foreach ($csv as $index => $row) {
            $io->progressAdvance();
            $entity = new $class();
            $entity->setInseeCode($row[0]);
            $entity->setLabel($row[3]);

            $this->entityManager->persist($entity);

            // Flush and clear the EntityManager at every batch
            if (($index + 1) % $batchSize === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
        }

        $this->entityManager->flush();
        $io->progressFinish();
    }
}
