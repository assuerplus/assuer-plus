<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\GarageReparation;
use App\Entity\RemorquageAutomobile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RemorquageAutomobileFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $garages = $manager->getRepository(GarageReparation::class)->findAll();

        foreach ($garages as $garage) {
            $remorquage = new RemorquageAutomobile();
            $remorquage->setNom('Service de remorquage de '.$garage->getNom());
            $remorquage->setAdresse('Adresse du service de remorquage');
            $remorquage->setTelephone('0123456789');
            $remorquage->setEmail('remorquage@example.com');
            $remorquage->setHeuresOuverture(new \DateTime('08:00'));
            $remorquage->setHeureFermeture(new \DateTime('18:00'));
            $remorquage->setGarageReparation($garage);

            $manager->persist($remorquage);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            GarageReparationFixtures::class,
        ];
    }
}
