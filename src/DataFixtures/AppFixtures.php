<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadCityFixtures($manager);
    }

    private function loadCityFixtures(ObjectManager $manager)
    {
        $cityFixtures = new CityFixtures();
        $cityFixtures->load($manager);
    }
}
