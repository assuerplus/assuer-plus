<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\GarageReparation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class GarageReparationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $cities = $manager->getRepository(City::class)->findAll();

        foreach ($cities as $city) {
            $garage = new GarageReparation();
            $garage->setNom('Garage de '.$city->getName());
            $garage->setAdresse('Adresse du garage '.$city->getName());
            $garage->setTelephone($this->generateRandomPhoneNumber());
            $garage->setEmail('garage@example.com');
            $garage->setSiteWeb('https://www.example.com');
            $garage->setDescription('Description du garage '.$city->getName());
            $garage->setHeuresOuverture(new \DateTime('08:00'));
            $garage->setHeureFermeture(new \DateTime('18:00'));
            $garage->setServicesOfferts('Services offerts par le garage '.$city->getName());
            $garage->setVille($city->getName());

            $manager->persist($garage);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CityFixtures::class,
        ];
    }

    private function generateRandomPhoneNumber(): string
    {
        $phoneNumber = '0';

        for ($i = 0; $i < 9; ++$i) {
            $phoneNumber .= mt_rand(0, 9);
        }

        return $phoneNumber;
    }
}
