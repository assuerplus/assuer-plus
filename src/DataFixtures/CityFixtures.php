<?php

namespace App\DataFixtures;

use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CityFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $citiesData = [
            [
                'name' => 'Paris',
                'code' => '75000',
            ],
            [
                'name' => 'Marseille',
                'code' => '13000',
            ],
            [
                'name' => 'Lyon',
                'code' => '69000',
            ],
            [
                'name' => 'Toulouse',
                'code' => '31000',
            ],
            [
                'name' => 'Nice',
                'code' => '06000',
            ],
            [
                'name' => 'Nantes',
                'code' => '44000',
            ],
            [
                'name' => 'Montpellier',
                'code' => '34000',
            ],
            [
                'name' => 'Strasbourg',
                'code' => '67000',
            ],
            [
                'name' => 'Bordeaux',
                'code' => '33000',
            ],
            [
                'name' => 'Lille',
                'code' => '59000',
            ],
            [
                'name' => 'Rennes',
                'code' => '35000',
            ],
            [
                'name' => 'Reims',
                'code' => '51100',
            ],
            [
                'name' => 'Saint-Étienne',
                'code' => '42000',
            ],
            [
                'name' => 'Toulon',
                'code' => '83000',
            ],
            [
                'name' => 'Le Havre',
                'code' => '76600',
            ],
            [
                'name' => 'Grenoble',
                'code' => '38000',
            ],
            [
                'name' => 'Dijon',
                'code' => '21000',
            ],
            [
                'name' => 'Angers',
                'code' => '49000',
            ],
            [
                'name' => 'Nîmes',
                'code' => '30000',
            ],
            [
                'name' => 'Villeurbanne',
                'code' => '69100',
            ],
            [
                'name' => 'Le Mans',
                'code' => '72000',
            ],
            [
                'name' => 'Aix-en-Provence',
                'code' => '13080',
            ],
            [
                'name' => 'Clermont-Ferrand',
                'code' => '63000',
            ],
            [
                'name' => 'Brest',
                'code' => '29200',
            ],
        ];

        foreach ($citiesData as $cityData) {
            $city = new City();
            $city->setName($cityData['name']);
            $city->setCode($cityData['code']);

            $manager->persist($city);
        }

        $manager->flush();
    }
}
