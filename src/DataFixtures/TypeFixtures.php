<?php

namespace App\DataFixtures;

use App\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $types = [
            [
                'nom' => 'Voiture',
                'marque' => 'Renault',
            ],
            [
                'nom' => 'Moto',
                'marque' => 'Honda',
            ],
            [
                'nom' => 'Camion',
                'marque' => 'Volvo',
            ],
            [
                'nom' => 'Bus',
                'marque' => 'Mercedes',
            ],
            [
                'nom' => 'Tracteur',
                'marque' => 'John Deere',
            ],
            [
                'nom' => 'Vélo',
                'marque' => 'Peugeot',
            ],
            [
                'nom' => 'Trottinette',
                'marque' => 'Xiaomi',
            ],
            [
                'nom' => 'Roller',
                'marque' => 'Rollerblade',
            ],
            [
                'nom' => 'Skateboard',
                'marque' => 'Element',
            ],
            [
                'nom' => 'Hoverboard',
                'marque' => 'Hoverboard',
            ],
            [
                'nom' => 'Trottinette électrique',
                'marque' => 'Xiaomi',
            ],
            [
                'nom' => 'Vélo électrique',
                'marque' => 'Peugeot',
            ],
            [
                'nom' => 'Moto électrique',
                'marque' => 'Honda',
            ],
            [
                'nom' => 'Voiture électrique',
                'marque' => 'Renault',
            ],
            [
                'nom' => 'Camion électrique',
                'marque' => 'Volvo',
            ],
            [
                'nom' => 'Bus électrique',
                'marque' => 'Mercedes',
            ],
            [
                'nom' => 'Tracteur électrique',
                'marque' => 'John Deere',
            ],
        ];

        foreach ($types as $typeData) {
            $type = new Type();
            $type->setNom($typeData['nom']);
            $type->setMarque($typeData['marque']);
            $type->setCreateAt(new \DateTimeImmutable());

            $manager->persist($type);
        }

        $manager->flush();
    }
}
