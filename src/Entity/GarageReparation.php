<?php

namespace App\Entity;

use App\Repository\GarageReparationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: GarageReparationRepository::class)]
class GarageReparation
{
    use Traits\HashIdTrait;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $adresse = null;

    #[ORM\Column(length: 255)]
    #[Assert\Regex(pattern: '/^0[1-9]([-. ]?[0-9]{2}){4}$/')]
    private ?string $telephone = null;

    #[ORM\Column(length: 255)]
    #[Assert\Email]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Assert\Url]
    private ?string $siteWeb = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $heuresOuverture = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $heureFermeture = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $servicesOfferts = null;

    #[ORM\OneToMany(mappedBy: 'GarageReparation', targetEntity: RemorquageAutomobile::class)]
    private Collection $remorquageAutomobiles;

    #[ORM\Column(length: 255)]
    private ?string $ville = null;

    public function __construct()
    {
        $this->remorquageAutomobiles = new ArrayCollection();
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSiteWeb(): ?string
    {
        return $this->siteWeb;
    }

    public function setSiteWeb(string $siteWeb): self
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getHeuresOuverture(): ?\DateTimeInterface
    {
        return $this->heuresOuverture;
    }

    public function setHeuresOuverture(?\DateTimeInterface $heuresOuverture): self
    {
        $this->heuresOuverture = $heuresOuverture;

        return $this;
    }

    public function getHeureFermeture(): ?\DateTimeInterface
    {
        return $this->heureFermeture;
    }

    public function setHeureFermeture(?\DateTimeInterface $heureFermeture): self
    {
        $this->heureFermeture = $heureFermeture;

        return $this;
    }

    public function getServicesOfferts(): ?string
    {
        return $this->servicesOfferts;
    }

    public function setServicesOfferts(?string $servicesOfferts): self
    {
        $this->servicesOfferts = $servicesOfferts;

        return $this;
    }

    /**
     * @return Collection<int, RemorquageAutomobile>
     */
    public function getRemorquageAutomobiles(): Collection
    {
        return $this->remorquageAutomobiles;
    }

    public function addRemorquageAutomobile(RemorquageAutomobile $remorquageAutomobile): self
    {
        if (!$this->remorquageAutomobiles->contains($remorquageAutomobile)) {
            $this->remorquageAutomobiles->add($remorquageAutomobile);
            $remorquageAutomobile->setGarageReparation($this);
        }

        return $this;
    }

    public function removeRemorquageAutomobile(RemorquageAutomobile $remorquageAutomobile): self
    {
        if ($this->remorquageAutomobiles->removeElement($remorquageAutomobile)) {
            // set the owning side to null (unless already changed)
            if ($remorquageAutomobile->getGarageReparation() === $this) {
                $remorquageAutomobile->setGarageReparation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RemorquageAutomobile>
     */
    public function getRemorquageAutomobile(): Collection
    {
        return $this->remorquageAutomobiles;
    }

    public function __toString(): string
    {
        return $this->nom;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }
}
