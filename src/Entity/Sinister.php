<?php

namespace App\Entity;

use App\Repository\SinisterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SinisterRepository::class)]
class Sinister
{
    use Traits\HashIdTrait;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'le lieu du sinistre ne peut être vide')]
    private ?string $lieux = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Length(
        min: 20,
        minMessage: 'La déscription ne peut pas faire moins de {{ limit }} caractères',
    )]
    #[Assert\NotBlank(message: 'La description ne peut pas être vide')]
    private ?string $description = null;

    #[ORM\Column(length: 9)]
    #[Assert\NotBlank(message : 'le numéro de plaque ne peut être vide')]
    #[Assert\Regex(
        pattern: '/[a-zA-Z]{2}[-][0-9]{3}[-][a-zA-Z]{2}/',
        message : 'Le numéro de plaque d\'immatriculation doit être au formmat aa-111-aa')]
    private ?string $numberRegistration = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\OneToMany(mappedBy: 'sinister', targetEntity: Images::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $Images;

    #[Assert\Count(max: 3, maxMessage: 'Le nombre de photo est limité à {{ limit }}')]
    #[Assert\All([
        new Assert\Image(mimeTypesMessage: 'Le fichier join n\'est pas une image'),
    ])]
    private $imagesFiles;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?GarageReparation $GarageReparation = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?RemorquageAutomobile $RemorquageAutomobile = null;

    #[ORM\ManyToOne(targetEntity: City::class)]
    #[ORM\JoinColumn(nullable: false)]
    private City $city;

    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'sinisters')]
    private Collection $Sinister;

    #[ORM\ManyToMany(targetEntity: self::class, mappedBy: 'Sinister')]
    private Collection $sinisters;

    #[ORM\ManyToOne(inversedBy: 'siniter')]
    private ?Type $type = null;

    #[ORM\Column]
    private string $marque;

    public function getMarque(): string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): void
    {
        $this->marque = $marque;
    }

    /**
     * __construct.
     *
     * @return void
     */
    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->Images = new ArrayCollection();
        $this->Sinister = new ArrayCollection();
        $this->sinisters = new ArrayCollection();
    }

    public function getLieux(): ?string
    {
        return $this->lieux;
    }

    public function setLieux(string $lieux): self
    {
        $this->lieux = $lieux;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumberRegistration(): ?string
    {
        return $this->numberRegistration;
    }

    public function setNumberRegistration(string $numberRegistration): self
    {
        $this->numberRegistration = $numberRegistration;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getImages(): ?Collection
    {
        return $this->Images;
    }

    public function addImage(Images $image): self
    {
        if (!$this->Images->contains($image)) {
            $this->Images->add($image);
            $image->setSinister($this);
        }

        return $this;
    }

    public function removeImage(Images $image): self
    {
        if ($this->Images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getSinister() === $this) {
                $image->setSinister(null);
            }
        }

        return $this;
    }

    public function getImagesFiles()
    {
        return $this->imagesFiles;
    }

    public function setImagesFiles($imagesFiles): self
    {
        foreach ($imagesFiles as $imageFile) {
            $image = new Images();
            $image->setImageFile($imageFile);
            $this->addImage($image);
        }
        $this->imagesFiles = $imagesFiles;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getGarageReparation(): ?GarageReparation
    {
        return $this->GarageReparation;
    }

    public function setGarageReparation(?GarageReparation $GarageReparation): self
    {
        $this->GarageReparation = $GarageReparation;

        return $this;
    }

    public function getRemorquageAutomobile(): ?RemorquageAutomobile
    {
        return $this->RemorquageAutomobile;
    }

    public function setRemorquageAutomobile(?RemorquageAutomobile $RemorquageAutomobile): self
    {
        $this->RemorquageAutomobile = $RemorquageAutomobile;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getSinister(): Collection
    {
        return $this->Sinister;
    }

    public function addSinister(self $sinister): self
    {
        if (!$this->Sinister->contains($sinister)) {
            $this->Sinister->add($sinister);
        }

        return $this;
    }

    public function removeSinister(self $sinister): self
    {
        $this->Sinister->removeElement($sinister);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getSinisters(): Collection
    {
        return $this->sinisters;
    }

    public function removeType(Type $type): self
    {
        $this->type->removeElement($type);

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }
}
