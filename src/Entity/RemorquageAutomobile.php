<?php

namespace App\Entity;

use App\Repository\RemorquageAutomobileRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RemorquageAutomobileRepository::class)]
class RemorquageAutomobile
{
    use Traits\HashIdTrait;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $adresse = null;

    #[ORM\Column(length: 255)]
    private ?string $telephone = null;

    #[ORM\Column(length: 255)]
    private ?string $email = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $heuresOuverture = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $heureFermeture = null;

    #[ORM\ManyToOne(inversedBy: 'remorquageAutomobiles')]
    private ?GarageReparation $GarageReparation = null;

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHeuresOuverture(): ?\DateTimeInterface
    {
        return $this->heuresOuverture;
    }

    public function setHeuresOuverture(?\DateTimeInterface $heuresOuverture): self
    {
        $this->heuresOuverture = $heuresOuverture;

        return $this;
    }

    public function getHeureFermeture(): ?\DateTimeInterface
    {
        return $this->heureFermeture;
    }

    public function setHeureFermeture(?\DateTimeInterface $heureFermeture): self
    {
        $this->heureFermeture = $heureFermeture;

        return $this;
    }

    public function getGarageReparation(): ?GarageReparation
    {
        return $this->GarageReparation;
    }

    public function setGarageReparation(?GarageReparation $GarageReparation): self
    {
        $this->GarageReparation = $GarageReparation;

        return $this;
    }

    public function __toString(): string
    {
        return $this->nom;
    }
}
