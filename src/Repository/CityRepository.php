<?php

namespace App\Repository;

use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    /**
     * Recherche une ville par son code INSEE.
     *
     * @param string $inseeCode Le code INSEE de la ville
     *
     * @return City|null La ville correspondante ou null si non trouvée
     */
    public function findByInseeCode(string $inseeCode): ?City
    {
        return $this->findOneBy(['inseeCode' => $inseeCode]);
    }

    /**
     * Recherche les villes d'un département.
     *
     * @param string $departmentNumber Le numéro du département
     *
     * @return City[] Les villes du département
     */
    public function findByDepartmentNumber(string $departmentNumber): array
    {
        return $this->findBy(['departmentNumber' => $departmentNumber]);
    }

    /**
     * Recherche les villes d'une région.
     *
     * @param string $regionName Le nom de la région
     *
     * @return City[] Les villes de la région
     */
    public function findByRegionName(string $regionName): array
    {
        return $this->findBy(['regionName' => $regionName]);
    }
}
