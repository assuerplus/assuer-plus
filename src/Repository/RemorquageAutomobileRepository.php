<?php

namespace App\Repository;

use App\Entity\RemorquageAutomobile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RemorquageAutomobile>
 *
 * @method RemorquageAutomobile|null find($id, $lockMode = null, $lockVersion = null)
 * @method RemorquageAutomobile|null findOneBy(array $criteria, array $orderBy = null)
 * @method RemorquageAutomobile[]    findAll()
 * @method RemorquageAutomobile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RemorquageAutomobileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RemorquageAutomobile::class);
    }

    public function save(RemorquageAutomobile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RemorquageAutomobile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return RemorquageAutomobile[] Returns an array of RemorquageAutomobile objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RemorquageAutomobile
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
