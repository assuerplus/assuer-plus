<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(name: 'app:users')]
class CreateUserCommand extends Command
{
    public function __construct(private EntityManagerInterface $entityManager, private UserPasswordHasherInterface $passwordEncoder)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = 'password';
        // Vérifier si un utilisateur avec l'adresse e-mail fournie existe déjà
        $existingUser = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email]);
        if ($existingUser) {
            $output->writeln('Un utilisateur avec cette adresse e-mail existe déjà.');

            return Command::FAILURE;
        }

        // Créer un nouvel utilisateur avec le rôle de super admin
        $user = new User();
        $user->setEmail('user@studi.fr');
        $user->setRoles(['ROLE_CLIENT']);
        $user->setUsername('superadmin');
        $user->setNumberUser('123456');
        $user->setPhoneNumber('0606060606');
        $user->setLastname('superadmin');
        $user->setFirstname('superadmin');

        // Générer un mot de passe sécurisé pour l'utilisateur
        $plainPassword = 'password';
        $encodedPassword = $this->passwordEncoder->hashPassword($user, $plainPassword);
        $user->setPassword($encodedPassword);

        // Enregistrer l'utilisateur dans la base de données
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        // Afficher les informations de l'utilisateur créé
        $output->writeln('Super admin user created successfully:');
        $output->writeln('Email: '.$user->getEmail());
        $output->writeln('Password: '.$plainPassword);

        return Command::SUCCESS;
    }
}
