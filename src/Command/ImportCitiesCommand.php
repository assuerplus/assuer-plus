<?php

namespace App\Command;

use App\Service\CsvConverterService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:import-cities')]
class ImportCitiesCommand extends Command
{
    public function __construct(
        private CsvConverterService $csvConverterService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->csvConverterService->ImportCitites($io);
        $io->success('Cities imported successfully');

        return Command::SUCCESS;
    }
}
