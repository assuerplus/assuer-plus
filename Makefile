# Variables
PHP=php
COMPOSER=composer
PHP_CS_FIXER=vendor/bin/php-cs-fixer fix src
PHPSTAN=vendor/bin/phpstan analyze src
PHPUNIT=vendor/bin/phpunit


# Règles
.PHONY: install fix analyze test

install:
	$(COMPOSER) install

fix:
	$(PHP) $(PHP_CS_FIXER) 

analyze:
	$(PHP) $(PHPSTAN) 

test:
	$(PHP) $(PHPUNIT)


cities:
	CURL -L -o import/cities.csv https://www.data.gouv.fr/fr/datasets/r/51606633-fb13-4820-b795-9a2a575a72f1
.PHONY: cities



drop:
	$(PHP) bin/console doctrine:database:drop --force
.PHONY: drop


create:
	$(PHP) bin/console doctrine:database:create
.PHONY: create


migrate:
	$(PHP) bin/console doctrine:migrations:migrate
.PHONY: migrate

migration:
	$(PHP) bin/console make:migration
.PHONY: migration


fixtures:
	$(PHP) bin/console doctrine:fixtures:load
.PHONY: fixtures

clear:
	$(PHP) bin/console cache:clear
.PHONY: clear


super-admin:
	php bin/console app:super-admin
.PHONY: super-admin


users:
	php bin/console app:users
.PHONY: users


lint:
	$(PHP) vendor/bin/php-cs-fixer fix src
.PHONY: lint